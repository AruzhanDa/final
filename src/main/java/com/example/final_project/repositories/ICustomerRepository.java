package com.example.final_project.repositories;

import com.example.final_project.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ICustomerRepository extends JpaRepository<Customer, Long> {
}
