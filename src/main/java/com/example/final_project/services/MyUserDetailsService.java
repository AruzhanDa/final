package com.example.final_project.services;

import com.example.final_project.MyUserDetails;
import com.example.final_project.entities.User;
import com.example.final_project.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.getUserByUsername(s);
        if (user == null) throw new UsernameNotFoundException("Could not find an username: " + s);
        return new MyUserDetails(user);
    }
}
