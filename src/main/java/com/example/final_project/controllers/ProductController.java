package com.example.final_project.controllers;

import com.example.final_project.entities.Product;
import com.example.final_project.repositories.IProductRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    private final IProductRepository repository;

    public ProductController(IProductRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/get/products")
    List<Product> all() {
        return repository.findAll();
    }

    @PostMapping("/create/products")
    Product newProduct(@RequestBody Product newProduct) {
        return repository.save(newProduct);
    }

    @GetMapping("/get/products/{id}")
    Product one(@PathVariable Long id) throws Exception {
        return repository.findById(id)
                .orElseThrow(() -> new Exception("Product not found"));
    }

    @PutMapping("/edit/products/{id}")
    Product replaceCustomer(@RequestBody Product newProduct, @PathVariable Long id) {
        return repository.findById(id)
                .map(product -> {
                    product.setProductName(newProduct.getProductName());
                    product.setAmount(newProduct.getAmount());
                    return repository.save(product);
                })
                .orElseGet(() -> {
                    newProduct.setId(id);
                    return repository.save(newProduct);
                });
    }

    @DeleteMapping("/delete/products/{id}")
    void deleteProduct(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
