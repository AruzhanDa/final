package com.example.final_project.entities;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @Column
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    @Getter
    private String name;
}
