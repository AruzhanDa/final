package com.example.final_project.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {
    @Id
    @GeneratedValue
    @Getter
    @Setter
    @Column(name = "customer_id")
    private Long id;

    @Getter
    @Setter
    @Column
    private String customerName;

    @Getter
    @Setter
    @Column
    private String phone;

    @Getter
    @Setter
    @Column
    private String email;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) && Objects.equals(customerName, customer.customerName) && Objects.equals(phone, customer.phone) && Objects.equals(email, customer.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerName, phone, email);
    }
}
