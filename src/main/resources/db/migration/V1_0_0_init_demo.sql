CREATE TABLE IF NOT EXISTS users
(
    id        SERIAL PRIMARY KEY,
    username  VARCHAR(20)  NOT NULL,
    password  VARCHAR(300) NOT NULL,
    fist_name VARCHAR(20)  NOT NULL,
    last_name VARCHAR(20)  NOT NULL
);

CREATE TABLE IF NOT EXISTS roles
(
    role_id SERIAL PRIMARY KEY,
    name    VARCHAR(45) NOT NULL
);

CREATE TABLE IF NOT EXISTS users_roles
(
    user_id INT NOT NULL,
    role_id INT NOT NULL,
    CONSTRAINT users_fk
        FOREIGN KEY (user_id)
            REFERENCES users (id),
    CONSTRAINT roles_fk
        FOREIGN KEY (role_id)
            REFERENCES roles (role_id)
);

CREATE TABLE IF NOT EXISTS roles
(
    role_id SERIAL PRIMARY KEY,
    name    VARCHAR(45) NOT NULL
);

CREATE TABLE IF NOT EXISTS customer
(
    customer_id   SERIAL PRIMARY KEY,
    customer_name VARCHAR(20) NOT NULL,
    phone         VARCHAR(10),
    email         VARCHAR(20)
);

CREATE TABLE IF NOT EXISTS product
(
    product_id   SERIAL PRIMARY KEY,
    product_name VARCHAR(20) NOT NULL,
    amount       INT         NOT NULL
);